﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Design_Patterns_Project_RPPOON
{
    public partial class UserDataWindow : Form
    {

        List<Product> products;
        User user;

        public UserDataWindow(List<Product> products,User user)
        {
            InitializeComponent();

            
            this.products = products;
            this.user = user;
            this.Text = user.getEmail() + " account";
            subscribeBtn.Enabled = false;

            LoadProductList();
            
        }

        public void LoadProductList()
        {
            this.productListBox.DataSource = null;
            this.productListBox.DataSource = products;
        }

        private void subscribeBtn_Click(object sender, EventArgs e)
        {
            Product selectedItem = (Product)productListBox.SelectedItem;
            selectedItem.attachPriceObserver(this.user);

            System.Windows.Forms.MessageBox.Show("You have subscribed to this item's price change notifications");
        }

        private void messageViewBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show(user.getMessage());
        }

        private void logOutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void productListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            subscribeBtn.Enabled = true;
        }

        private void clearMessagesBtn_Click(object sender, EventArgs e)
        {
            user.clearMessages();
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            LoadProductList();
        }

        private void unsubscribeBtn_Click(object sender, EventArgs e)
        {
            Product selectedItem = (Product)productListBox.SelectedItem;
            selectedItem.detachPriceObserver(this.user);

            System.Windows.Forms.MessageBox.Show("You won't be recieving notifications for this item's price change any more");
        }
    }
}
