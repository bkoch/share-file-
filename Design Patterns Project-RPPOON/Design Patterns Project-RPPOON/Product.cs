﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class Product
    {
        private string name;
        private double price;
        private List<IProductObserver> priceObservers;

        public Product(string name,double price)
        {
            this.name = name;
            this.price = price;
            priceObservers = new List<IProductObserver>();
        }

        public void attachPriceObserver(User user)
        {
            priceObservers.Add(user);
        }

        public void detachPriceObserver(User user)  
        {
            priceObservers.Remove(user);
        }

        public void reducePrice(double discountPercentage) 
        {   
            if(discountPercentage<100 && discountPercentage>0)
            {
                this.price = this.price  * ((100 - discountPercentage) / 100);

                string discountMessage = this.name + " is discounted by: " + discountPercentage.ToString() + " percent";
                NotifyObservers(discountMessage);
            }
            else
            {
                throw new Exception();
            }
        }

        private void NotifyObservers(string message)
        {
            foreach(IProductObserver observer in priceObservers)
            {
                observer.Update(message);
            }
        }

        public override string ToString()
        {
            return "Name: "+name + " Price: "+ price;

        }


    }
}
