﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class Users
    {
        private Dictionary<string, User> users;

        public Users()
        {
            users = new Dictionary<string, User>();
        }

        public void registerUser(User user,PasswordValidator passwordValidator,EmailValidator emailValidator)
        {
            if (!passwordValidator.validatePassword(user.getPassword()) || !emailValidator.validateEmail(user.getEmail()) )  
            {
                throw new Exception("Wrong email or password format");
                
            }
            if(users.ContainsKey(user.getEmail()))
            {
                throw new Exception("Email adress already in use");
            }
            users.Add(user.getEmail(), user);
        }

        public User getUserInfo(string email,string password)
        {
            if (users[email].getPassword() == password)
            {
                return users[email];
            }
            else
            {
                throw new Exception();
            }
        }

        
    }
}
