﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Design_Patterns_Project_RPPOON
{
    public partial class LoginWindow : Form
    {
        List<Product> products = new List<Product>();
        Users users = new Users();
        PasswordValidator passwordValidator;
        EmailValidator emailValidator;


        public LoginWindow()
        {
            InitializeComponent();
            passwordTextBox.PasswordChar = '*';

            //PASSWORD VALIDATOR SETUP
            StringUppercaseChecker stringUpperCaseChecker = new StringUppercaseChecker();
            StringDigitChecker stringDigitChecker = new StringDigitChecker();
            StringLowercaseChecker stringLowerCaseChecker = new StringLowercaseChecker();
            StringLengthChecker stringLengthChecker = new StringLengthChecker();

            passwordValidator = new PasswordValidator(stringDigitChecker);
            //passwordValidator.addValidatorModule(stringLowerCaseChecker);
            passwordValidator.addValidatorModule(stringLengthChecker);
            //passwordValidator.addValidatorModule(stringUpperCaseChecker);

            //EMAIL VALIDATOR SETUP
            StringEmailExtensionChecker stringEmailExtensionChecker = new StringEmailExtensionChecker();
            StringAtSymbolChecker stringAtSymbolChecker = new StringAtSymbolChecker();
            StringStartingWithLetterChecker stringStartingWithLetterChecker = new StringStartingWithLetterChecker();

            emailValidator = new EmailValidator(stringAtSymbolChecker);
            emailValidator.addValidatorModule(stringEmailExtensionChecker);
            emailValidator.addValidatorModule(stringStartingWithLetterChecker);


            //INITIAL DATA SETUP
            Product laptop1 = new Product("dell laptop", 1000);
            Product laptop2 = new Product("asus laptop", 600);
            Product laptop3 = new Product("huawei laptop", 700);

            products.Add(laptop1);
            products.Add(laptop2);
            products.Add(laptop3);

            User BrandoKoch = new User("bkoch@etfos.hr", "12345", true);
            users.registerUser(BrandoKoch,passwordValidator,emailValidator);

            User MarkoMarkovic = new User("mmarkovic@etfos.hr", "12345");
            users.registerUser(MarkoMarkovic,passwordValidator,emailValidator);

        }

        private void userRegisterBtn_Click(object sender, EventArgs e)
        {
            string email = emailTextBox.Text.ToString();
            string password = passwordTextBox.Text.ToString();

            try
            {
                users.registerUser(new User(email, password), passwordValidator, emailValidator);
                System.Windows.Forms.MessageBox.Show("Registration Complete");
            }
            catch(Exception ex)
            {   
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
            
        }

        private void userLoginButton_Click(object sender, EventArgs e)
        {
            string email = emailTextBox.Text.ToString();
            string password = passwordTextBox.Text.ToString();

            
            try
            {
                User user = users.getUserInfo(email, password);

                if (!user.isUserAdmin())
                {
                    UserDataWindow userDataWindow = new UserDataWindow(products, user);
                    userDataWindow.Show();
                }
                else
                {
                    AdminDataWindow adminDataWindow = new AdminDataWindow(products);
                    adminDataWindow.Show();
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Wrong password or email!");
            }

            
        }

      
    }
}
