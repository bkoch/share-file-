﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class StringAtSymbolChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            int atSymbolCount = 0;
            foreach (char c in stringToCheck)
            {
                if (c == '@')
                {
                    atSymbolCount += 1;
                }
            }

            if(atSymbolCount==1)
            {
                return true;
            }
            return false;
            
        }
    }
}
