﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Design_Patterns_Project_RPPOON
{
    public partial class AdminDataWindow : Form
    {
        List<Product> products;

        public AdminDataWindow(List<Product> products)
        {
            InitializeComponent();

            this.products = products;
            reducePriceBtn.Enabled = false;
            

            LoadProductList();
        }

        public void LoadProductList()
        {
            this.productListBox.DataSource = null;
            this.productListBox.DataSource = products;
        }

        private void reducePriceBtn_Click(object sender, EventArgs e)
        {
            string text = percentageTextBox.Text;
            double percentage = Convert.ToDouble(text);

            percentageTextBox.Clear();

            try
            {   
                
                Product product = (Product)productListBox.SelectedItem;
                product.reducePrice(percentage);
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Percentage should be between 1-100");
            }

            LoadProductList();
        }

        private void logOutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void productListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            reducePriceBtn.Enabled = true;
            removeProductBtn.Enabled = true;
        }

        private void addProductBtn_Click(object sender, EventArgs e)
        {
            Product newProduct = new Product(this.nameTextBox.Text, Convert.ToDouble(priceTextBox.Text));

            if (!string.IsNullOrWhiteSpace(nameTextBox.Text) && !string.IsNullOrWhiteSpace(priceTextBox.Text) )
            {
                this.products.Add(newProduct);

                LoadProductList();

                nameTextBox.Clear();
                priceTextBox.Clear();
            }


            
        }

        private void removeProductBtn_Click(object sender, EventArgs e)
        {
            Product product = (Product)productListBox.SelectedItem;
            products.Remove(product);

            LoadProductList();
            
            if(products.Count<1)
            {
                removeProductBtn.Enabled = false;
                reducePriceBtn.Enabled = false;
            }
        }
    }
}
