﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class PasswordValidator
    {
        StringChecker firstStringChecker;

        public PasswordValidator(StringChecker firstStringChecker)
        {
            this.firstStringChecker = firstStringChecker;
        }

        public void addValidatorModule(StringChecker inputStringChecker)
        {
            inputStringChecker.SetNext(this.firstStringChecker);
            firstStringChecker = inputStringChecker;
        }

        public bool validatePassword(string password)
        {
            return firstStringChecker.Check(password);
        }
    }
}
