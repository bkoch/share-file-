﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class User:IProductObserver
    {
        private string email;
        private string password;
        private bool isAdmin;
        private string messages;

        public User(string email,string password,bool isAdmin=false)
        {
            this.email = email;
            this.password = password;
            this.isAdmin = isAdmin;
            this.messages = "";
        }

        public string getPassword()
        {
            return password;
        }

        public string getEmail()
        {
            return email;
        }

        public bool isUserAdmin()
        {
            return isAdmin;
        }

        public string getMessage()
        {
            return messages;
        }

        void IProductObserver.Update(string message)
        {
            this.messages += "\n"+ DateTime.Now.ToString()+ ": "  + message;
        }

        public void clearMessages()
        {
            messages = "";
        }
    }
}
