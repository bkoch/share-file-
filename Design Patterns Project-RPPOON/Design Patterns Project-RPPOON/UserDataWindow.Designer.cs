﻿namespace Design_Patterns_Project_RPPOON
{
    partial class UserDataWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subscribeBtn = new System.Windows.Forms.Button();
            this.messageViewBtn = new System.Windows.Forms.Button();
            this.logOutBtn = new System.Windows.Forms.Button();
            this.productListBox = new System.Windows.Forms.ListBox();
            this.clearMessagesBtn = new System.Windows.Forms.Button();
            this.refreshBtn = new System.Windows.Forms.Button();
            this.unsubscribeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // subscribeBtn
            // 
            this.subscribeBtn.Location = new System.Drawing.Point(29, 312);
            this.subscribeBtn.Name = "subscribeBtn";
            this.subscribeBtn.Size = new System.Drawing.Size(138, 52);
            this.subscribeBtn.TabIndex = 1;
            this.subscribeBtn.Text = "Subscribe";
            this.subscribeBtn.UseVisualStyleBackColor = true;
            this.subscribeBtn.Click += new System.EventHandler(this.subscribeBtn_Click);
            // 
            // messageViewBtn
            // 
            this.messageViewBtn.Location = new System.Drawing.Point(540, 73);
            this.messageViewBtn.Name = "messageViewBtn";
            this.messageViewBtn.Size = new System.Drawing.Size(190, 56);
            this.messageViewBtn.TabIndex = 2;
            this.messageViewBtn.Text = "View Messages";
            this.messageViewBtn.UseVisualStyleBackColor = true;
            this.messageViewBtn.Click += new System.EventHandler(this.messageViewBtn_Click);
            // 
            // logOutBtn
            // 
            this.logOutBtn.Location = new System.Drawing.Point(536, 240);
            this.logOutBtn.Name = "logOutBtn";
            this.logOutBtn.Size = new System.Drawing.Size(194, 53);
            this.logOutBtn.TabIndex = 3;
            this.logOutBtn.Text = "Log out";
            this.logOutBtn.UseVisualStyleBackColor = true;
            this.logOutBtn.Click += new System.EventHandler(this.logOutBtn_Click);
            // 
            // productListBox
            // 
            this.productListBox.FormattingEnabled = true;
            this.productListBox.ItemHeight = 24;
            this.productListBox.Location = new System.Drawing.Point(29, 73);
            this.productListBox.Name = "productListBox";
            this.productListBox.Size = new System.Drawing.Size(493, 220);
            this.productListBox.TabIndex = 4;
            this.productListBox.SelectedIndexChanged += new System.EventHandler(this.productListBox_SelectedIndexChanged);
            // 
            // clearMessagesBtn
            // 
            this.clearMessagesBtn.Location = new System.Drawing.Point(540, 160);
            this.clearMessagesBtn.Name = "clearMessagesBtn";
            this.clearMessagesBtn.Size = new System.Drawing.Size(190, 48);
            this.clearMessagesBtn.TabIndex = 5;
            this.clearMessagesBtn.Text = "Clear Messages";
            this.clearMessagesBtn.UseVisualStyleBackColor = true;
            this.clearMessagesBtn.Click += new System.EventHandler(this.clearMessagesBtn_Click);
            // 
            // refreshBtn
            // 
            this.refreshBtn.Location = new System.Drawing.Point(29, 20);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(123, 47);
            this.refreshBtn.TabIndex = 6;
            this.refreshBtn.Text = "Refresh List";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // unsubscribeBtn
            // 
            this.unsubscribeBtn.Location = new System.Drawing.Point(364, 312);
            this.unsubscribeBtn.Name = "unsubscribeBtn";
            this.unsubscribeBtn.Size = new System.Drawing.Size(158, 52);
            this.unsubscribeBtn.TabIndex = 7;
            this.unsubscribeBtn.Text = "Unsubscribe";
            this.unsubscribeBtn.UseVisualStyleBackColor = true;
            this.unsubscribeBtn.Click += new System.EventHandler(this.unsubscribeBtn_Click);
            // 
            // UserDataWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 378);
            this.Controls.Add(this.unsubscribeBtn);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.clearMessagesBtn);
            this.Controls.Add(this.productListBox);
            this.Controls.Add(this.logOutBtn);
            this.Controls.Add(this.messageViewBtn);
            this.Controls.Add(this.subscribeBtn);
            this.Name = "UserDataWindow";
            this.Text = "User Access Data";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button subscribeBtn;
        private System.Windows.Forms.Button messageViewBtn;
        private System.Windows.Forms.Button logOutBtn;
        private System.Windows.Forms.ListBox productListBox;
        private System.Windows.Forms.Button clearMessagesBtn;
        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.Button unsubscribeBtn;
    }
}