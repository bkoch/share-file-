﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class StringDigitChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            foreach (char c in stringToCheck)//provjeri
            {
                if (Char.IsDigit(c))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
