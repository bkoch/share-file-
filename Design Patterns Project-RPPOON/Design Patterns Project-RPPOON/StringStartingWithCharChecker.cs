﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    class StringStartingWithLetterChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            return ((stringToCheck[0] >= 'A' && stringToCheck[0] <= 'Z') || (stringToCheck[0] >= 'a' && stringToCheck[0] <= 'z'));
        }
    }
}
