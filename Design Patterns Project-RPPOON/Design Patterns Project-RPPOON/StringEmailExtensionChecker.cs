﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class StringEmailExtensionChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            int stringSize = stringToCheck.Length;
            if( stringToCheck[stringSize-3]=='.' && stringToCheck[stringSize-2]=='h' && stringToCheck[stringSize-1] == 'r')
            {
                return true;
            }
            return false;
        }
    }
}
