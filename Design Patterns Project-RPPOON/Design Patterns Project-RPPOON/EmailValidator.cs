﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Patterns_Project_RPPOON
{
    public class EmailValidator
    {
        StringChecker firstStringChecker;

        public EmailValidator(StringChecker firstStringChecker)
        {
            this.firstStringChecker = firstStringChecker;
        }

        public void addValidatorModule(StringChecker inputStringChecker)
        {
            inputStringChecker.SetNext(this.firstStringChecker);
            firstStringChecker = inputStringChecker;
        }

        public bool validateEmail(string email)
        {
            return firstStringChecker.Check(email);
        }
    }
}
