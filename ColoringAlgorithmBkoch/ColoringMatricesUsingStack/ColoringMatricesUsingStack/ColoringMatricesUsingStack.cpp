// ColoringMatricesUsingStack.cpp : Defines the entry point for the console application.
//by Brando Koch

//This algorithm "paints" the matrix(parts of the matrix) to wantedColorValue starting from one point and than advancing to every adjacent point of the matrix that has the same color as the startingPointColor 
//Similar to bucket tool in Paint program 
//In this context adjacent point means up,down,left,or right  -no diagonal relation 



#include "stdafx.h"
#include "Header.h"//includes the custom implemented stack class and functions 
#define N 5 //number of rows
#define M 10//number of columns

void printMatrix(const int matrix[N][M]) 
{
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < M; ++j)
		{
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl << endl << endl;
}

void paintMatrix(int matrix[N][M],point startingPoint,int wantedColorValue)
{
	Stack stack(100);//100 is just to ensure enough space 
	int startingPointColor = matrix[startingPoint.x][startingPoint.y];//we document the current color so we know on which fields the wanted color will expand(it expends an all adjacent points which have the same color as the currentColor)
	stack.Push(startingPoint);
	
	while(!stack.is_empty())//we break from this when the stack becomes empty or to say everything is colored
	{	
		point pointPop = stack.Pop();
		matrix[pointPop.x][pointPop.y] = wantedColorValue;
		if(pointPop.y-1>=0 && matrix[pointPop.x][pointPop.y-1]==startingPointColor)//checks if up of the current point is startingPointColor and we havent crossed matrix borders
		{
			stack.Push(point{pointPop.x,pointPop.y - 1 });//if it is that means it needs to be colored so this puts it in the stack
		}
		if(pointPop.x-1>=0 && matrix[pointPop.x-1][pointPop.y] == startingPointColor)//checks if left of the current point is startingPointColor and we havent crossed matrix borders
		{
			stack.Push(point{ pointPop.x-1,pointPop.y});//if it is that means it needs to be colored so this puts it in the stack
		}
		if (pointPop.y + 1<=9 && matrix[pointPop.x][pointPop.y+1] == startingPointColor)//checks if down of the current point is startingPointColor and we havent crossed matrix borders
		{
			stack.Push(point{ pointPop.x,pointPop.y+1 });//if it is that means it needs to be colored so this puts it in the stack
		}
		if (pointPop.x + 1 <= 4 && matrix[pointPop.x+1][pointPop.y] == startingPointColor)//checks if right of the current point is startingPointColor and we havent crossed matrix borders
		{
			stack.Push(point
			
			{ pointPop.x+1,pointPop.y });//if it is that means it needs to be colored so this puts it in the stack
		}

		//printMatrix(matrix); //remove comment to see how the painting executes one coloration at a time
	}

	cout << endl << endl << endl;
}


int main()
{		
						   //numeration of columns
						   //0,1,2,3,4,5,6,7,8,9  
	int matrix[N][M] = {	{0,0,0,0,0,0,0,0,0,0},//0
	/*[rows][colums]*/		{2,2,2,2,2,2,2,2,2,2},//1
							{0,0,0,2,0,0,0,0,0,0},//2    //numeration of rows
							{0,0,0,2,2,2,0,0,0,0},//3
							{0,0,0,0,0,0,0,0,0,0}};//4 


	point startingPointOfColoration{ 2,4 };
	int wantedColorValue = 1;

	printMatrix(matrix);//print default state
	paintMatrix(matrix, startingPointOfColoration, wantedColorValue);
	printMatrix(matrix);//print colored state

    return 0;
}












