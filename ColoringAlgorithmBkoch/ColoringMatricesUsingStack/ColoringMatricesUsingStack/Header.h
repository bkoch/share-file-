#pragma once
#include "stdafx.h"
#include <iostream>	
using namespace std;

//STRUCTURE: POINT
struct point
{
	int x;
	int y;
};


//CUSTOM STACK IMPLEMENTATION
//stack pointer first advances than adds the element to the stack thats why its default is -1
class Stack
{
private:
	int size;
	int stackPointer;//stack pointer
	point* arrayPointer;
public:
	Stack(int Size) :size(Size), stackPointer(-1), arrayPointer(new point[Size]) {}
	void Push(point x);
	point Pop();
	void Clear();
	bool is_empty();
	bool is_full();
};

void Stack::Push(point x)
{
	if (is_full())
	{
		cout << "Error: stack is full";
		exit(1);
	}
	else
	{
		stackPointer += 1;//if its the first push it advances form -1 to 0 
		arrayPointer[stackPointer] = x;
	}
}

point Stack::Pop()
{
	point popValue;
	if (is_empty())
	{
		cout << "Error:the stack is empty already you cannot pop" << endl;
		exit(1);
	}
	else
	{
		popValue = arrayPointer[stackPointer];//save value to popValue to return later
		stackPointer -= 1;//decrement stackPointer
	}
	return popValue;
}


bool Stack::is_empty()
{
	if (stackPointer == -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Stack::is_full()
{
	if (stackPointer == (size - 1))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Stack::Clear()
{
	stackPointer = -1;
}






